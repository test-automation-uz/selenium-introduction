from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from webdriver_manager.chrome import ChromeDriverManager

# Initialize the WebDriver
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

# Navigate to a website
driver.get("https://www.example.com")

# Find an input element on the webpage
input_element = driver.find_element(By.CSS_SELECTOR, 'div > div > button')

# Clear any existing text in the input field (optional)
input_element.clear()

# Send keys to the input field
input_element.send_keys('Hello, Selenium!')

# Simulate pressing Enter key
input_element.send_keys(Keys.ENTER)

# Simulate pressing Tab key
input_element.send_keys(Keys.TAB)

# Simulate holding down the Shift key while typing
ActionChains(driver).key_down(Keys.SHIFT).send_keys('hello').key_up(Keys.SHIFT).perform()

# Using ActionChains to perform complex keyboard actions
actions = ActionChains(driver)
actions.key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL)  # Select all text
actions.context_click(input_element).send_keys(Keys.ARROW_DOWN).send_keys(Keys.ENTER)  # Right-click and select an option
actions.perform()

# Using combination of keys
input_element.send_keys(Keys.CONTROL + 'a')  # Select all text
input_element.send_keys(Keys.CONTROL + 'c')  # Copy selected text
input_element.send_keys(Keys.CONTROL + 'v')  # Paste copied text

# Close the browser window
driver.quit()
