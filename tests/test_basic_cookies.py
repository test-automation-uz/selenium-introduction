import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from selenium_introduction.cookies_helper import read_cookies, write_cookies


class TestBasicCookies:

    def test_get_cookies(self, driver):
        driver.maximize_window()
        driver.get("http://demo.guru99.com/test/social-icon.html")
        cookies = driver.get_cookies()
        for cookie in cookies:
            print(f"{cookie['name']}, ")

    def test_add_cookie(self, driver):
        driver.get("http://demo.guru99.com/test/cookie/selenium_aut.php")
        cookie = {'name': 'foo', 'value': 'bar'}
        driver.add_cookie(cookie)
        driver_cookie = driver.get_cookie('foo')
        assert driver_cookie['value'] == 'bar'

    def test_delete_cookie(self, driver):
        driver.get("http://demo.guru99.com/test/cookie/selenium_aut.php")
        cookie = {'name': 'foo', 'value': 'bar'}
        driver.add_cookie(cookie)
        lp_cookie = driver.get_cookie('foo')
        assert lp_cookie is not None
        driver.delete_cookie('foo')
        deleted_cookie = driver.get_cookie('foo')
        assert deleted_cookie is None

    def test_delete_all_cookies(self, driver):
        driver.get("http://demo.guru99.com/test/cookie/selenium_aut.php")
        driver.find_element(By.NAME, "username").send_keys("abc123")
        driver.find_element(By.NAME, "password").send_keys("123xyz")
        driver.find_element(By.NAME, "submit").click()

        cookies = driver.get_cookies()
        for ck in cookies:
            print(f"{ck['name']} {ck['value']}")
            driver.delete_cookie(ck['name'])

        after_deleting_cookies = driver.get_cookies()
        for ck in after_deleting_cookies:
            print(f"Not deleted cookies {ck['name']}")

    def test_save_cookies(self, driver):
        driver.get("http://demo.guru99.com/test/cookie/selenium_aut.php")

        # Input Email id and Password If you are already registered
        driver.find_element(By.NAME, "username").send_keys("abc123")
        driver.find_element(By.NAME, "password").send_keys("123xyz")
        driver.find_element(By.NAME, "submit").click()

        assert driver.find_element(By.CSS_SELECTOR, ".form-signin-heading center").text == "You are logged In"

        cookies = driver.get_cookies()
        selenium_cookie = next(cookie for cookie in cookies if cookie['name'] == 'Selenium')
        print(len(cookies))
        write_cookies(cookies)

        driver.close()

        # Restore all cookies from the previous session
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.get("http://demo.guru99.com/test/cookie/selenium_aut.php")
        driver.delete_cookie(selenium_cookie['name'])
        driver.add_cookie(selenium_cookie)
        driver.refresh()
        assert driver.find_element(By.CSS_SELECTOR, ".form-signin-heading center").text == "You are logged In"
        driver.delete_all_cookies()

    def test_write_cookies(self, driver):
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.get("https://demo.guru99.com/test/cookie/selenium_aut.php")

        driver.find_element(By.NAME, "username").send_keys("abc123")
        driver.find_element(By.NAME, "password").send_keys("123xyz")
        driver.find_element(By.NAME, "submit").click()

        time.sleep(2)

        cookies = driver.get_cookies()
        selenium_cookies = next(cookie for cookie in cookies if cookie['name'] == 'Selenium')
        print(selenium_cookies)

    def test_read_cookies(self, driver):

        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        driver.get("https://demo.guru99.com/test/cookie/selenium_aut.php")

        driver.delete_all_cookies()

        cookies_file = read_cookies()

        for cookie in cookies_file:
            driver.add_cookie(cookie)

        time.sleep(2)

        driver.refresh()

        time.sleep(2)

        cookies = driver.get_cookies()

        print(cookies)

        driver.close()
