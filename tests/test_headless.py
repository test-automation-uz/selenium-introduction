import os

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


def test_headless():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')  # Enable headless mode
    chrome_options.add_argument('--disable-gpu')  # Disable GPU acceleration in headless mode
    chrome_options.add_argument('--window-size=1920x1080')  # Set window size (optional)

    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)

    driver.get('http://techcanvass.com/Examples/webtable.html')

    screenshot_path = os.path.join(os.getcwd(), 'screenshot.png')

    driver.save_screenshot(screenshot_path)

    driver.quit()
