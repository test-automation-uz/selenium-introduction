import time

from selenium.common import TimeoutException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TestHoverExample:

    def test_hover_using_action_chains(self, driver):
        from selenium.webdriver.common.action_chains import ActionChains
        actions = ActionChains(driver)
        element_to_hover = driver.find_element(By.ID, "hover_element_id")
        actions.move_to_element(element_to_hover).perform()
