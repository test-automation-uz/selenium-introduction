import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_scroll_actions():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

    driver.get("https://getbootstrap.com/docs/5.0/getting-started/introduction/")

    element = driver.find_element(By.CSS_SELECTOR, 'h3#html5-doctype')
    print(element.text)

    # actions = ActionChains(driver)
    #
    # actions.move_to_element(element).perform()

    # driver.execute_script('''
    # var wa = document.getElementById('html5-doctype')
    # wa.scrollIntoView(true)
    # ''')

    driver.execute_script('''
    window.scrollTo(0, document.body.scrollHeight);
    ''')

    time.sleep(2)


def test_scroll_actions():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

    driver.get("http://webdriveruniversity.com/Scrolling/index.html")

    script = driver.execute_script('''
    return document.getElementById('zone2-entries').innerText;
    ''')
    print(str(script))
    time.sleep(2)


def test_arguments_pass_to_execute_script():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

    driver.get("http://webdriveruniversity.com/Scrolling/index.html")
    element = driver.find_element(By.ID, 'zone4')
    driver.execute_script("arguments[0].scrollIntoView();",element)

    driver.execute_script('''
    arguments[0].scrollIntoView();
    ''', element)

    time.sleep(2)
