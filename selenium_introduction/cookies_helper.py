import os
import pickle
import random
import string
from pathlib import Path


def write_cookies(cookies):
    folder_path = 'cookies'
    file_path = os.path.join(folder_path, 'data.pkl')
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    serialized_data = pickle.dumps(cookies)
    with open(file_path, 'wb') as file:
        file.write(serialized_data)


def read_cookies():
    folder_path = 'cookies'
    file_path = os.path.join(folder_path, 'data.pkl')

    if os.path.exists(file_path):
        with open(file_path, 'rb') as file:
            data = pickle.load(file)
            print(f'Contents of data.pkl:\n{data}')
            return data


def generate_random_user_and_password():
    username_chars = string.ascii_letters + string.digits
    password_chars = string.ascii_letters + string.digits + string.punctuation

    username_length = 15
    password_length = 15

    username = ''.join(random.choice(username_chars) for _ in range(username_length))
    password = ''.join(random.choice(password_chars) for _ in range(password_length))

    print("Generated username:", username)
    print("Generated password:", password)

    return username, password
