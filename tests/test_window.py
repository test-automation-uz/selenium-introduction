import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class TestWindow:
    def test_window(self, driver):
        driver.get("https://seleniumhq.github.io")

        wait = WebDriverWait(driver, 10)

        original_window = driver.current_window_handle

        driver.find_element(By.CSS_SELECTOR, "#announcement-banner > div > div > div > h4 > a").click()

        wait.until(EC.number_of_windows_to_be(2))

        for window_handle in driver.window_handles:
            if window_handle != original_window:
                driver.switch_to.window(window_handle)
                break
        time.sleep(3)
        # print(driver.title)
        # wait.until(EC.title_is("SeleniumConf Chicago 2023 Playlist"))

        # driver.switch_to.window(driver.window_handles[0])

    # htmltable = driver.find_element(By.XPATH, "//*[@id='main']/table[1]/tbody")
    # rows = htmltable.find_elements(By.TAG_NAME, "tr")
    #
    # for rnum in range(len(rows)):
    #     columns = rows[rnum].find_elements(By.TAG_NAME, "th")
    #     print("Number of columns:", len(columns))
    #
    #     for cnum in range(len(columns)):
    #         print(columns[cnum].text)

    def test_new_windows(self, driver):
        driver.get("https://seleniumhq.github.io")

        for i in range(0, 3):
            driver.switch_to.new_window("tab")
            # driver.switch_to.new_window("window")
            # driver.execute_script("window.open('');")
            print(i)
            time.sleep(2)

            # http://testautomationpractice.blogspot.com/