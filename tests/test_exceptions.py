import time

from selenium.common import TimeoutException, NoAlertPresentException, NoSuchWindowException, WebDriverException, \
    StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TestPopup:

    def test_frame(self, driver):
        driver.get("https://demoqa.com/alerts")

        driver.find_element(By.ID, 'timerAlertButton').click()

        try:
            WebDriverWait(driver, 6).until(expected_conditions.alert_is_present())
            driver.switch_to.alert.accept()
            time.sleep(2)
            driver.switch_to.alert.accept()

        except NoAlertPresentException as e:
            print("No alert")
            print(e.msg)
        except TimeoutException as e:
            print(e.msg)

        #
        # driver.find_element(By.ID, 'timerAlertButton').click()
        #
        # time.sleep(8)

        # from selenium.common.exceptions import NoSuchElementException
        #
        # try:
        #     elem = driver.find_element(By.XPATH, ".//*[@id='SOME_ACTION']")
        #     elem.click()
        # except NoSuchElementException as e:
        #     elem = driver.find_element(By.XPATH, ".//*[@id='ALTERNATIVE_ACTION']")
        #     elem.click()

        # from selenium.common import NoSuchWindowException
        #
        # try:
        #     driver.switch_to.window('new_window')
        # except NoSuchWindowException:
        #     print("No such window exists.")
        # #     do some other actions
        # finally:
        #     driver.quit()

        # def test_frame_switching(self):
        #     driver.get("your_url_here")
        #     WebDriverWait(driver, 3).until(expected_conditions.frame_to_be_available_and_switch_to_it("frame_11"))
        #
        #     try:
        #         self.driver.switch_to.frame("frame_11")
        #     except WebDriverException as e:
        #         print("An exceptional case")
        #         print(e.msg)

        # def test_handle_alerts(self):
        #
        #     try:
        #         wait = WebDriverWait(driver, 10)
        #         wait.until(expected_conditions.alert_is_present())
        #
        #         try:
        #             driver.switch_to.alert.accept()
        #         except NoAlertPresentException as e:
        #             print("An exceptional case")
        #             print(e.msg)
        #
        #     except TimeoutException as e:
        #         print("WebDriver couldn’t locate the Alert")
        #         print(e.msg)
        #
        #     wait = WebDriverWait(driver, 30)
        #     wait.until(lambda d: d.execute_script("return document.readyState") == "complete")
        #
        #     try:
        #         driver.find_element(By.XPATH, "//*[contains(@id,'firstname')]").send_keys("Aaron")
        #     except StaleElementReferenceException as e:
        #         print(f"StaleElementReferenceException occurred: {e}")

        # Define the asynchronous script to wait for an element to appear
        # language=javascript
        # async_script = """
        #     var callback = arguments[arguments.length - 1];
        #
        #     // Example: Wait for an element with id 'exampleElement' to be present
        #     var waitForElement = setInterval(function() {
        #         var element = document.getElementById('exampleElement');
        #         if (element) {
        #             clearInterval(waitForElement);
        #             callback(element);
        #         }
        #     }, 100);
        # """
        #
        # # Execute the asynchronous script and get the result
        # element = driver.execute_async_script(async_script)
        #
        # # Now you can use the 'element' in your further automation logic
        # print("Found element:", element.text)




