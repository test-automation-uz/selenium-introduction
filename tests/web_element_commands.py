import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from webdriver_manager.chrome import ChromeDriverManager


class TestWebElementCommands:
    driver = None

    @classmethod
    def setup_class(cls):
        chrome_service = ChromeService(ChromeDriverManager().install())
        cls.driver = webdriver.Chrome(service=chrome_service)

    @classmethod
    def teardown_class(cls):
        cls.driver.quit()

    def test_get_text_and_clear(self):
        self.driver.get("https://the-internet.herokuapp.com/inputs")
        self.driver.maximize_window()
        element = self.driver.find_element(By.XPATH, "/html/body/div[2]/div/div/div/div/input")
        print(f"Get text: {element.text}")
        assert self.driver.find_element(By.XPATH, "/html/body/div[2]/div/div/div/div/p").text == "Number"
        element.send_keys("1")
        element.clear()

    def test_get_location(self):
        self.driver.get("http://the-internet.herokuapp.com/drag_and_drop")
        self.driver.maximize_window()
        element_a = self.driver.find_element(By.ID, "column-a").location['x']
        element_b = self.driver.find_element(By.ID, "column-b").location['x']
        print(element_a, element_b)
        assert element_a < element_b

    def test_check_element_states(self):
        self.driver.get("https://the-internet.herokuapp.com/checkboxes")
        self.driver.maximize_window()
        element = self.driver.find_element(By.CSS_SELECTOR, "#checkboxes > input[type=checkbox]:nth-child(3)")
        element.click()

        print("isEnabled: " + str(element.is_enabled()))
        print("isDisplayed: " + str(element.is_displayed()))
        print("isSelected: " + str(element.is_selected()))

        assert element.is_enabled()
        assert element.is_displayed()
        assert not element.is_selected()

    def test_get_info(self):
        self.driver.get("http://the-internet.herokuapp.com/drag_and_drop")
        self.driver.maximize_window()
        element = self.driver.find_element(By.ID, "column-a")

        dimension = element.size

        print(f"Height: {str(dimension['height'])} \nWidth: {str(dimension['width'])}")
        print(f"Css value: {element.value_of_css_property("background-color")}")

        assert dimension['height'] > 0
        assert dimension['width'] > 0
        assert element.value_of_css_property("background-color")

        point = element.location

        print(f"X coordinate: {str(point['x'])} \nY coordinate: {str(point['y'])}")
        print("getAttribute: " + element.get_attribute("id"))
        print("getTagName: " + element.tag_name)

        assert point['x'] >= 0
        assert point['y'] >= 0

        assert element.get_attribute("id")
        assert element.tag_name

    def test_submit_form(self):
        self.driver.get("http://the-internet.herokuapp.com/forgot_password")
        self.driver.maximize_window()
        element = self.driver.find_element(By.ID, "email")
        element.send_keys("nz@gmail.com")
        element.send_keys(Keys.RETURN)

        time.sleep(3)

    def test_go_to_link(self):
        self.driver.get("http://the-internet.herokuapp.com/javascript_alerts")
        self.driver.maximize_window()
        element = self.driver.find_element(By.CLASS_NAME, "example")
        element.click()

    def test_find_elements(self):
        self.driver.get("http://the-internet.herokuapp.com/add_remove_elements/")
        self.driver.maximize_window()
        add_button = self.driver.find_element(By.XPATH, "/html/body/div[2]/div/div/button")

        for i in range(2):
            add_button.click()

        element = self.driver.find_element(By.CSS_SELECTOR, ".added-manually")
        print(element.location)
        assert element.location
        # add_button_1 = self.driver.find_element(By.CLASS_NAME, "added-manually")
        all_add_buttons = self.driver.find_elements(By.XPATH, "/html/body/div[2]/div/div/div/button")

        for add_button in all_add_buttons:
            add_button.click()

    def test_get_info_1(self):
        self.driver.get("http://the-internet.herokuapp.com/drag_and_drop")
        self.driver.maximize_window()
        element_a = self.driver.find_element(By.ID, "column-a")
        element_b = self.driver.find_element(By.ID, "column-b")

        point_a = element_a.location
        assert point_a['x'] >= 0
        assert point_a['y'] >= 0

        point_b = element_b.location
        assert point_b['x'] >= 0
        assert point_b['y'] >= 0


    def test_check_radio_button(self):
        self.driver.get("https://techcanvass.com/examples/register.html")
        self.driver.maximize_window()
        male = self.driver.find_element(By.CSS_SELECTOR, "input[value='male']")
        female = self.driver.find_element(By.CSS_SELECTOR, "input[value='female']")
        female.click()
        if not female.is_selected():
            female.click()

    def test_dropdowns(self):
        self.driver.get("http://webdriveruniversity.com/Dropdown-Checkboxes-RadioButtons/index.html")
        self.driver.maximize_window()
        checkboxes = self.driver.find_elements(By.XPATH, "//div[@id='checkboxes']/label")
        for checkbox in checkboxes:
            checkbox.click()


    def test_select_option_from_dropdown(self):
        self.driver.get("https://techcanvass.com/examples/register.html")
        self.driver.maximize_window()
        dropdown = Select(self.driver.find_element(By.CSS_SELECTOR, "select[name='model']"))
        dropdown.select_by_value("Ser64")
        print("multiple", dropdown.is_multiple)

        selected_options = [option.text for option in dropdown.options]
        for selected_option in selected_options:
            print(selected_option)

    def test_get_option_from_dropdown_angular(self):
        self.driver.get("https://ng-bootstrap.github.io/#/components/dropdown/examples")
        self.driver.maximize_window()
        self.driver.find_element(By.ID, "dropdownBasic1").click()
        dropdown_options = self.driver.find_elements(By.XPATH, "//div[@class='component-demo']/div[@class='card']/div[@class='card-body']//div[@ngbdropdown]/button[@id='dropdownBasic1']")

    def test_parameter(self):
        self.driver.get("http://demo.guru99.com/V4/");
        user_name = self.driver.find_element(By.NAME, "uid")
        user_name.send_keys("guru99")
        password = self.driver.find_element(By.NAME, "password")
        password.send_keys("guru99")
        print(self.driver.find_element(By.XPATH, "//*[@name='btnLogin']").text)


    def test_alert(self):
        self.driver.get("http://demo.guru99.com/V4/");
        user_name = self.driver.find_element(By.NAME, "uid")
        user_name.send_keys("guru99")
        password = self.driver.find_element(By.NAME, "password")
        password.send_keys("guru99")
        print(self.driver.find_element(By.XPATH, "//*[@name='btnLogin']").text)
        self.driver.find_element(By.CSS_SELECTOR, "input[type='submit'][value='LOGIN']").submit()
        time.sleep(2)
        alert = Alert(self.driver)
        alert_text = alert.text
        print("Alert Text:", alert_text)
        time.sleep(2)
        # Accept the alert (click OK)
        alert.accept()

        # Dismiss the alert (click Cancel)
        # alert.dismiss()
