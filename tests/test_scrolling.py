import time

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By


class TestScrolling:
    def test_do_scroll(self, driver):
        driver.maximize_window()
        driver.get("http://webdriveruniversity.com/Scrolling/index.html")

        js = driver.execute_script("return document.getElementById('zone2-entries').innerText;")
        checkbox_text = str(js)
        print(checkbox_text)

    def test_scroll(self, driver):
        driver.get("https://getbootstrap.com/docs/5.0/getting-started/introduction/")
        time.sleep(2)
        element = driver.find_element(By.CSS_SELECTOR, "h3#html5-doctype")
        print(element.text)
        # action = ActionChains(driver)
        #
        # action.move_to_element(element).perform()
        # time.sleep(2)

        # js = driver.execute_script('''
        # var wikipediaLink = document.querySelector("h3#html5-doctype");
        # wikipediaLink.scrollIntoView(true);
        # ''')

        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        time.sleep(2)