from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


def test_get_info():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

    driver.get("http://the-internet.herokuapp.com/windows")
    driver.maximize_window()

    print("Title ", driver.title)
    print("URL ", driver.current_url)
    print("Page Source ", driver.page_source)
    print("Page Source Contains", "A/B Testing" in driver.page_source)

    driver.find_element(By.XPATH, "//*[text()='Click Here']").click()
    driver.quit()


def test_alerts():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    driver.implicitly_wait(2)

    driver.get("https://www.demoblaze.com")
    driver.maximize_window()

    driver.find_element(By.XPATH, "/html/body/div[5]/div/div[2]/div/div[5]/div/div/h4/a").click()
    element: WebElement = driver.find_element(By.ID, "myTabContent")
    print(element.text)
    driver.find_element(By.XPATH, "/html/body/div[5]/div/div[2]/h3").is_displayed()


def test_find_all_product_name():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    driver.implicitly_wait(2)

    driver.get("https://www.demoblaze.com")
    driver.maximize_window()

    elements: list[WebElement] = driver.find_elements(By.CSS_SELECTOR, "div div h4 a")
    all_titles = [element.text for element in elements]
    next_button: WebElement = driver.find_element(By.CSS_SELECTOR, "button#next2")
    next_button.click()
    wait = WebDriverWait(driver, timeout=10)
    # wait.until(lambda d: d.execute_script("return document.readyState") == "complete")
    wait.until(DivTextChange((By.CSS_SELECTOR, "#tbodyid > div:nth-child(1) > div > div > h4 > a"), elements[0].text))
    elements_page2: list[WebElement] = driver.find_elements(By.CSS_SELECTOR, "div div h4 a")

    all_titles += [element.text for element in elements_page2]

    for element in all_titles:
        print(element)


class DivTextChange(object):
    def __init__(self, locator, old_text):
        self.locator = locator
        self.old_text = old_text

    def __call__(self, driver):
        new_text = driver.find_element(*self.locator).text
        return new_text != self.old_text
