import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_css():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

    driver.get("http://the-internet.herokuapp.com/")

    all_odd_elements = driver.find_elements(By.CSS_SELECTOR, '#rso > div:nth-child(odd) > div div > div > span > a h3')
    all_even_elements = driver.find_elements(By.CSS_SELECTOR, '#rso > div:nth-child(even) > div div > div > span > a h3')
