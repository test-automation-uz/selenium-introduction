from selenium.webdriver.common.by import By


class TestTables:

    def test_handle_static_table(self, driver):
        driver.get("http://techcanvass.com/Examples/webtable.html")
        driver.maximize_window()

        for row_no in range(2, 5):
            for col_no in range(1, 4):
                cell_text = driver.find_element(By.XPATH, f"//*[@id='t01']/tbody/tr[{row_no}]/td[{col_no}]").text
                print(cell_text)

    def test_handle_dynamic_table(self, driver):
        driver.get("http://techcanvass.com/Examples/webtable.html")
        driver.maximize_window()

        web_table = driver.find_element(By.XPATH, "html/body/table")

        rows = web_table.find_elements(By.TAG_NAME, "tr")
        print("Number of Rows including headings:", len(rows))

        columns = rows[0].find_elements(By.TAG_NAME, "th")
        print("Number of columns:", len(columns))

        for r_num in range(1, len(rows) + 1):
            for col_num in range(1, len(columns) + 1):
                if r_num == 1:
                    cell_text = driver.find_element(By.XPATH, f"//*[@id='t01']/tbody/tr[{r_num}]/th[{col_num}]").text
                else:
                    cell_text = driver.find_element(By.XPATH, f"//*[@id='t01']/tbody/tr[{r_num}]/td[{col_num}]").text

                print(cell_text)
