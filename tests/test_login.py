import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from selenium_introduction.cookies_helper import write_cookies, read_cookies, generate_random_user_and_password


class TestLogin:
    def test_registration_and_save_credential_tokens(self, driver):
        driver.maximize_window()
        print(driver.get_cookies())

        user_name, password = generate_random_user_and_password()

        driver.get("https://www.demoblaze.com/index.html")
        time.sleep(2)
        driver.find_element(By.ID, "signin2").click()

        wait = WebDriverWait(driver, 3)
        wait.until(lambda d: (element := driver.find_element(By.ID, "signInModal")).get_attribute('class').__contains__(
            'show') and element.value_of_css_property('display') == 'block')

        driver.find_element(By.ID, "sign-username").send_keys(user_name)
        driver.find_element(By.ID, "sign-password").send_keys(password)
        driver.find_element(By.CSS_SELECTOR,
                            "#signInModal > div > div > div.modal-footer > button.btn.btn-primary").click()

        WebDriverWait(driver, 3).until(expected_conditions.alert_is_present())
        alert = driver.switch_to.alert
        assert alert.text == "Sign up successful."
        alert.accept()

        driver.find_element(By.ID, "login2").click()
        wait.until(lambda d: (element := driver.find_element(By.ID, "logInModal")).get_attribute('class').__contains__(
            'show') and element.value_of_css_property('display') == 'block')

        driver.find_element(By.ID, "loginusername").send_keys(user_name)
        driver.find_element(By.ID, "loginpassword").send_keys(password)

        driver.find_element(By.CSS_SELECTOR,
                            "#logInModal > div > div > div.modal-footer > button.btn.btn-primary").click()

        time.sleep(5)
        cookies = driver.get_cookies()

        write_cookies(cookies)
        print(cookies)

    def test_login_by_saved_cookies(self, driver):
        driver.get("https://www.demoblaze.com/index.html")
        driver.maximize_window()

        print(driver.get_cookies())

        cookies = read_cookies()
        for cookie in cookies:
            if cookie['name'] == 'user':
                driver.delete_cookie(cookie['name'])
            driver.add_cookie(cookie)

        driver.refresh()

        time.sleep(5)
        assert driver.find_element(By.CSS_SELECTOR, "#nameofuser").text.__contains__("Welcome")
        print(driver.get_cookies())
