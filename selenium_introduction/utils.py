import os
from pathlib import Path


def test_path():
    import os

    cwd = os.getcwd()
    print(f"Current working directory: {cwd}")

    pythonpath = os.environ['PYTHONPATH']
    print(f"PYTHONPATH: {pythonpath}")


def test_path2():
    project_root = find_project_root()
    print(f"project_root: {project_root}")


def find_project_root() -> [Path, None]:
    current_directory = Path.cwd()
    while current_directory != current_directory.parent:
        if (current_directory / 'pyproject.toml').is_file():
            return current_directory
        current_directory = current_directory.parent
    return None

def ensure_downloads_dir_exists():
    dir_path = Path(f"{Path.cwd()}/downloads")

    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
