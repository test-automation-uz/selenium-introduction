from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


def test_implicit_wait():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    driver.implicitly_wait(10)
    driver.get("http://the-internet.herokuapp.com/")

    dynamic_controls_page = driver.find_element(By.CSS_SELECTOR, "li a[href='/dynamic_controls']")
    # /dynamic_controls

    dynamic_controls_page.click()

    element = driver.find_element(By.CSS_SELECTOR, "form#checkbox-example #checkbox > input[type=checkbox]")

    element.click()

    assert element.is_selected() is True

    driver.find_element(By.CSS_SELECTOR, "#checkbox-example > button").click()

    driver.find_element(By.CSS_SELECTOR, "#checkbox-example #message").is_displayed()


def test_explicit_wai():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    driver.get("http://the-internet.herokuapp.com/")

    dynamic_controls_page = driver.find_element(By.CSS_SELECTOR, "li a[href='/dynamic_controls']")
    # /dynamic_controls

    dynamic_controls_page.click()

    element = driver.find_element(By.CSS_SELECTOR, "form#checkbox-example #checkbox > input[type=checkbox]")

    element.click()

    assert element.is_selected() is True

    driver.find_element(By.CSS_SELECTOR, "#checkbox-example > button").click()

    wait = WebDriverWait(driver, timeout=10)
    wait.until(lambda d: driver.find_element(By.CSS_SELECTOR, "#checkbox-example #message").is_displayed())

    message = driver.find_element(By.CSS_SELECTOR, "#checkbox-example #message")
    print(message.text)


def test_explicit_wait_of_progress_bar():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    driver.get("https://demoqa.com/progress-bar")

    driver.find_element(By.ID, "startStopButton").click()

    driver.find_element(By.CSS_SELECTOR, "#progressBar div")

    element = WebDriverWait(driver, 15).until(
        expected_conditions.presence_of_element_located((By.XPATH, '//div[@aria-valuenow="100"]'))
    )
    print(element.text)


def test_explicit_wait_of_progress_bar_custom_ec():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    driver.get("https://demoqa.com/progress-bar")

    driver.find_element(By.ID, "startStopButton").click()

    progress_bar = (By.CSS_SELECTOR, "#progressBar div")

    WebDriverWait(driver, 15).until(AriaValueNowEquals(progress_bar, '100'))


def explicit_wait_example():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    WebDriverWait(driver, 10).until(
        expected_conditions.element_to_be_clickable((By.ID, 'element_id'))

    )
    WebDriverWait(driver, 10).until(
        expected_conditions.visibility_of_element_located((By.ID, 'element_id'))
    )


class AriaValueNowEquals(object):
    def __init__(self, locator, expected_value):
        self.locator = locator
        self.expected_value = expected_value

    def __call__(self, driver):
        e = driver.find_element(*self.locator)
        aria_value_now = e.get_attribute("aria-valuenow")
        return aria_value_now == self.expected_value
