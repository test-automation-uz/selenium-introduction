import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


def test_get_info():
    chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument('--incognito')
    # chrome_options.add_argument('--headless')
    chrome_options.add_argument('--')
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)

    driver.get("http://the-internet.herokuapp.com/windows")
    driver.maximize_window()

    print("Title ", driver.title)
    print("URL ", driver.current_url)
    print("Page Source ", driver.page_source)
    print("Page Source Contains", "A/B Testing" in driver.page_source)

    driver.find_element(By.XPATH, "//*[text()='Click Here']").click()
    driver.quit()

def test_ssl():

    # https://expired.badssl.com/
    chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument('--incognito')
    # chrome_options.add_argument('--headless')
    # chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--window-size=1920x1080')
    chrome_options.page_load_strategy("eager")
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
    # driver.set_window_size(1920, 1080)

    driver.get("https://expired.badssl.com/")

    time.sleep(3)
    driver.quit()
