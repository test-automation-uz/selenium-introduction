import time

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(params=["desktop", "mobile", "tablet"], scope="class")
def device(request):
    return request.param


@pytest.fixture(scope="class")
def driver(request, device):
    chrome_options = webdriver.ChromeOptions()

    if device == "mobile":
        chrome_options.add_experimental_option("mobileEmulation", {"deviceName": "iPhone XR"})
    if device == "tablet":
        chrome_options.add_experimental_option("mobileEmulation", {"deviceName": "iPad"})

    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)

    def teardown():
        driver.quit()

    request.addfinalizer(teardown)

    return driver


class TestBrowser:

    def test_1(self, driver):
        driver.get("https://the-internet.herokuapp.com/")
        time.sleep(3)

    def test_2(self, driver):
        driver.get("https://www.demoblaze.com/index.html")
        time.sleep(3)

    def test_3(self, driver):
        driver.get("https://techcanvass.com/examples/register.html")
        time.sleep(3)
