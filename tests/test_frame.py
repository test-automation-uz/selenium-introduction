import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class TestWindow:

    def frame_functions(self, driver):
        driver.switch_to.frame(0)
        driver.switch_to.frame("frame-name")
        iframe = driver.find_element(By.TAG_NAME, "iframe")
        driver.switch_to.frame(iframe)

        WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.ID, "frame-id")))

        driver.switch_to.default_content()

    def test_frame(self, driver):
        driver.get("https://testautomationpractice.blogspot.com/")
        iframe = driver.find_element(By.CSS_SELECTOR, "#frame-one796456169")
        driver.switch_to.frame(iframe)

        time.sleep(3)

        input = driver.find_element(By.CSS_SELECTOR, "input#RESULT_TextField-0")
        input.send_keys("Nika")

        time.sleep(3)
        driver.switch_to.default_content()

    def test_parent_frame(self, driver):
        driver.get("https://demoqa.com/nestedframes")
        driver.switch_to.frame("frame1")
        print(driver.find_element(By.XPATH, "//body").text)

        driver.switch_to.frame(0)
        print(driver.find_element(By.XPATH, "//body").text)

        driver.switch_to.parent_frame()
        print(driver.find_element(By.XPATH, "//body").text)

        driver.switch_to.default_content()
