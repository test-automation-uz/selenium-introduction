from selenium import webdriver
from selenium.webdriver.chrome.service import Service


def test_driver_config_by_chromedriver_file():
    service = Service(executable_path="../chromedriver.exe")
    options = webdriver.ChromeOptions()
    # options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(options=options, service=service)
