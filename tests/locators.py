from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

# By.ID
page_footer = driver.find_element(By.ID, "page-footer")

# By.XPATH
page_footer = driver.find_element(By.XPATH, "/html/body/div[3]")

# By.TAG_NAME
element = driver.find_element(By.TAG_NAME, "h3")

# By.LINK_TEXT
h3_element = driver.find_element(By.LINK_TEXT, "A/B Test Control")

# By.PARTIAL_LINK_TEXT
h3_element = driver.find_element(By.PARTIAL_LINK_TEXT, "Cont")

# By.NAME
element = driver.find_element(By.NAME, "your_element_name")

# By.CLASS_NAME
element = driver.find_element(By.CLASS_NAME, "example")

# By.CSS_SELECTOR
element = driver.find_element(By.CSS_SELECTOR, "#page-footer")
