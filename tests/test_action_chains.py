import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_mouse_actions():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

    driver.get("http://the-internet.herokuapp.com/")

    hover_element = driver.find_element(By.LINK_TEXT, 'Hovers')
    actions = ActionChains(driver)
    actions.move_to_element(hover_element).click().perform()

    another_element = driver.find_element(By.CSS_SELECTOR, '#content > div > div:nth-child(5) > img')
    actions.move_to_element(another_element).perform()

    actions.release()

    time.sleep(3)

    users = driver.find_elements(By.CSS_SELECTOR, "#content > div img")

    for user in users:
        time.sleep(5)
        actions.move_to_element(user).perform()

    driver.quit()
