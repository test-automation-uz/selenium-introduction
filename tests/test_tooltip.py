from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By


class TestTooltip:
    def test_handle_tooltip(self, driver):
        driver.maximize_window()
        driver.get("http://demo.guru99.com/test/social-icon.html")
        expected_tooltip = "Github"

        # github = driver.find_element(By.XPATH, ".//*[@class='soc-ico show-round']/a[4]")
        # github = driver.find_element(By.XPATH, '//*[@id="page"]/div/div/a')
        github = driver.find_element(By.XPATH, '//*[@id="page"]/div[2]/div/a[4]')
        actual_tooltip = github.get_attribute("title")

        print("Actual Title of Tool Tip:", actual_tooltip)
        assert actual_tooltip == expected_tooltip, "Test Case Failed"

    def test_handle_tooltip_with_click_and_hold(self, driver):
        driver.maximize_window()
        driver.get("http://demo.guru99.com/test/tooltip.html")
        expected_tooltip = "What's new in 3.2"

        download = driver.find_element(By.XPATH, ".//*[@id='download_now']")
        actions = ActionChains(driver)
        actions.click_and_hold(download).perform()

        tooltip_element = driver.find_element(By.XPATH, ".//*[@class='box']/div/a")
        actual_tooltip = tooltip_element.text

        print("Actual Title of Tool Tip:", actual_tooltip)
        assert actual_tooltip == expected_tooltip, "Test Case Failed"

    def test_handle_to_do(self, driver):
        driver.maximize_window()
        driver.get("http://webdriveruniversity.com/To-Do-List/index.html")

        download = driver.find_element(By.XPATH, "//*[contains(text(),'Buy new robes')]")
        actions = ActionChains(driver)
        actions.click_and_hold(download).perform()

        tooltip_element = driver.find_element(By.XPATH, "//*[contains(text(),'Buy new robes')]//i")
        tooltip_element.click()
