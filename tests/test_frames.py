import time

from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait


class TestFrames:
    # def setup_class(self, driver):
    #     driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    #
    # def teardown_class(self, driver):
    #     driver.quit()

    def test_switch_to_frame(self, driver):
        driver.get("https://demoqa.com/frames")
        driver.maximize_window()
        driver.switch_to.frame("frame1")
        print(driver.find_element(By.ID, "sampleHeading").text)
        driver.switch_to.default_content()
        print(driver.find_element(By.CSS_SELECTOR, "#framesWrapper div").text)

    def test_switch_to_nested_frames(self, driver):
        driver.get("http://the-internet.herokuapp.com/nested_frames")
        driver.maximize_window()

        driver.switch_to.frame("frame-top")
        driver.switch_to.frame("frame-left")

        driver.switch_to.default_content()
        print(driver.find_element(By.NAME, "frame-top").get_attribute("name"))

    def test_handle_alerts(self, driver):
        driver.get("https://demoqa.com/alerts")
        driver.maximize_window()
        driver.find_element(By.ID, "promtButton").click()

        WebDriverWait(driver, 5).until(expected_conditions.alert_is_present())
        # alert = driver.switch_to.alert
        alert = Alert(driver)
        alert.send_keys("boot camp")
        alert.accept()

        assert driver.find_element(By.ID, "promptResult").text == "You entered boot camp"

    def test_catch_windows(self, driver):
        driver.maximize_window()
        driver.get("https://demoqa.com/browser-windows")

        driver.find_element(By.ID, "windowButton").click()
        main_window = driver.window_handles[0]
        for window_handle in driver.window_handles[1:]:
            driver.switch_to.window(window_handle)
            text = driver.find_element(By.ID, "sampleHeading").text
            print(f"Heading of child window is {text}")
            time.sleep(3)
            driver.close()

            print("Child window closed")

        driver.switch_to.window(main_window)

    def test_switch_to_active_element(self, driver):
        driver.maximize_window()
        driver.get("https://demoqa.com/browser-windows")
        driver.switch_to.active_element.click()
