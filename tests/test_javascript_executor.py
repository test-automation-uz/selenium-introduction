import time

from selenium.common import StaleElementReferenceException
from selenium.common.exceptions import NoAlertPresentException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class TestJavascriptExecutorDemo:

    def test_javascript_exe_method(self, driver):
        driver.get("https://www.gmail.com")
        next_button = driver.find_element(By.XPATH, "//*[@id='identifierNext']/div/button/span")

        js = driver.execute_script("document.getElementById('identifierId').value='n@gmail.com';")

        js = driver.execute_script("arguments[0].click();", next_button)

        # js = driver.execute_script("document.getElementById('enter element id').checked=false;")
        # language=javascript
        # Execute JavaScript code

        time.sleep(5)
        js_code = """
        var xpathResult = document.evaluate(
            "//div[contains(@aria-label, 'selected. Switch account')]/div[@jsname]",
            document,
            null,
            XPathResult.FIRST_ORDERED_NODE_TYPE,
            null
        );

        var node = xpathResult.singleNodeValue;
        return node.innerText;
        """

        result = str(driver.execute_script(js_code))

        # Print or use the result as needed
        print("Text content:", result)

        # s_text = driver.execute_script("return document.documentElement.innerText;")
        s_text = driver.execute_script("return document.documentElement.innerText;")
        print(s_text)

    def test_javascript_exe(self, driver):
        driver.get("http://testautomationpractice.blogspot.com/")
        search_input = driver.find_element(By.CSS_SELECTOR, "input.wikipedia-search-button[type='submit']")
        search = driver.find_element(By.CSS_SELECTOR, "input#Wikipedia1_wikipedia-search-input")

        js = driver.execute_script("document.getElementById('Wikipedia1_wikipedia-search-input').value='Selenium';")
        # var wikipediaLink = document.querySelector("#Wikipedia1_wikipedia-search-form > div > span.wikipedia-search-bar > span:nth-child(2) input");
        # wikipediaLink.click();
        js = driver.execute_script("arguments[0].click();", search_input)

        # js = driver.execute_script("document.getElementById('enter element id').checked=false;")
        # language=javascript
        # Execute JavaScript code

        time.sleep(5)

    def todo(self, driver):
        driver.manage().window().maximize()
        driver.get("http://gmail.com")

        js = driver.execute_script("document.getElementById('identifierId').value='nz@gmail.com';")
        login_button = driver.find_element(By.CLASS_NAME, "VfPpkd-RLmnJb")

        js = driver.execute_script("arguments[0].click();", login_button)

        checkbox = driver.find_element(By.CLASS_NAME, "jibhHc")
        checkbox_text = driver.execute_script("return arguments[0].innerText;", checkbox)
        print(checkbox_text)

    def test_do_scroll(self, driver):
        driver.get("http://webdriveruniversity.com/Scrolling/index.html")

        js = driver.execute_script("arguments[0].scrollIntoView();", driver.find_element(By.ID, "zone4"))

    def test_use_cases_for_js(self, driver):
        driver.get("http://webdriveruniversity.com/Scrolling/index.html")

        js = driver.execute_script("history.back();")
        js = driver.execute_script("history.forward();")
        js = driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
        js = driver.execute_script("window.location = 'https://demoqa.com/alerts'")

    def test_asinc(self, driver):
        driver.get("http://webdriveruniversity.com/Scrolling/index.html")
        element = driver.find_element(By.CSS_SELECTOR, "div#zone1")

        driver.execute_async_script("window.setTimeout(arguments[0], 5000)", "alert('Hello World!');")
        # https://www.tutorialspoint.com/how-to-execute-a-javascript-using-selenium-in-python

    def test_ecommerceplayground_staleelement(self, driver):

        driver.get('https://ecommerce-playground.lambdatest.io/index.php?route=account/login')

        emailElement = driver.find_element(By.ID, "input-email")
        passwordElement = driver.find_element(By.ID, "input-password")

        emailElement.send_keys("email@gmail.com")

        driver.find_element(By.XPATH, "//input[@type='submit']").click()

        try:
            passwordElement.send_keys("password")

        except StaleElementReferenceException:
            print('StaleElementReferenceException handled')
            passwordElement = driver.find_element(By.ID, "input-password")
            passwordElement.send_keys("password")

    def test_handle_exceptions(self, driver):
        driver.get("https://demoqa.com/alerts")
        driver.maximize_window()
        driver.find_element(By.ID, "timerAlertButton").click()

        try:
            WebDriverWait(driver, 4).until(EC.alert_is_present())
            driver.switch_to.alert.accept()
        except NoAlertPresentException as e:
            print("No alert")
            print(e.msg)
        except TimeoutException as e:
            print(e.msg)

        print("Out block")


# driver.execute_script("alert('Welcome To SoftwareTestingMaterial');")
# driver.execute_script("history.go(0);")
# text = driver.execute_script("return document.documentElement.innerText;")
#
# text = driver.execute_script("return document.title;")
# text = driver.execute_script("return document.domain;")
# text = driver.execute_script("return document.URL;")
#
# driver.execute_script("window.scrollBy(0,500);")
# driver.execute_script("window.scrollBy(0,document.body.scrollHeight);")
#
# driver.execute_script("$('ul.menus.menu-secondary.sf-js-enabled.sub-menu li').hover();")
# driver.execute_script("window.location = 'https://www.softwaretestingmaterial.com';")
#
# # Assuming 'element' is the hidden element you want to click
# element = driver.find_element(By.XPATH, "//xpath/to/your/hidden/element")
#
# # Click on the hidden element using JavaScriptExecutor
# driver.execute_script("arguments[0].click();", element)