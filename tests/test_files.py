import os
import time
from pathlib import Path

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

from selenium_introduction.utils import ensure_downloads_dir_exists


def test_download():
    # ensure_downloads_dir_exists()
    chrome_options = webdriver.ChromeOptions()
    # print(Path.cwd())
    default_downloads_dir = os.path.join(Path.cwd(), "downloads")
    print(default_downloads_dir)
    prefs = {
        "download.default_directory": default_downloads_dir,
    }

    chrome_options.add_experimental_option("prefs", prefs)

    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
    # driver.set_window_size(1920, 1080)

    driver.get("https://the-internet.herokuapp.com/download")
    driver.find_element(By.XPATH, '//a[text()="some-file.txt"]').click()

    time.sleep(5)

    driver.quit()


def test_upload():
    # ensure_downloads_dir_exists()
    chrome_options = webdriver.ChromeOptions()

    default_downloads_dir = os.path.join(Path.cwd(), "downloads")
    print(default_downloads_dir)
    prefs = {
        "download.default_directory": default_downloads_dir,
    }

    chrome_options.add_experimental_option("prefs", prefs)

    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
    # driver.set_window_size(1920, 1080)

    driver.get("https://demo.guru99.com/test/upload/")
    element = driver.find_element(By.NAME, 'uploadfile_0')

    element.send_keys(f"{Path.cwd()}/downloads/some-file.txt")
    driver.find_element(By.ID, "terms").click()

    driver.find_element(By.ID, "submitbutton").click()

    wait = WebDriverWait(driver, 10)
    wait.until(lambda m: m.find_element(By.CSS_SELECTOR, "#res > center").is_displayed())
    driver.quit()
