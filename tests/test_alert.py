import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.microsoft import EdgeChromiumDriverManager


def test_alter():
    driver = webdriver.Edge(service=Service(EdgeChromiumDriverManager().install()))

    driver.get("https://demoqa.com/alerts")

    click_button = driver.find_element(By.ID, 'alertButton')
    click_button.click()

    alert = driver.switch_to.alert

    alert.accept()

    time.sleep(30)


def test_alter_5_second_wait():
    driver = webdriver.Edge(service=Service(EdgeChromiumDriverManager().install()))

    driver.get("https://demoqa.com/alerts")

    click_button = driver.find_element(By.ID, 'timerAlertButton')
    click_button.click()

    WebDriverWait(driver, 6).until(expected_conditions.alert_is_present())

    alert = driver.switch_to.alert

    alert.accept()

    time.sleep(3)

