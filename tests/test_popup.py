import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class TestPopup:

    # language=xpath

    @staticmethod
    def get_locator(text):
        xpath = f"//div[@class='thumbnail']/h2[contains(text(),'{text}')]/..//span[@type='button']"
        locator = (By.XPATH, xpath)

        print(locator[1])
        return locator

    def test_popup(self, driver):
        driver.get("http://webdriveruniversity.com/Popup-Alerts/index.html")
        button = driver.find_element(*self.get_locator("Modal Popup"))

        button.click()

        wait = WebDriverWait(driver, 3)
        modal_locator = (By.CSS_SELECTOR, "div#myModal")
        wait.until(lambda d: (modal := d.find_element(*modal_locator)).get_attribute('class').__contains__('in') and modal.value_of_css_property('display') == 'block')

        # wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, '.modal.fade.in[style="display: block"]')))

        popup = driver.find_element(*modal_locator)

        h4 = popup.find_element(By.CSS_SELECTOR, "div.modal-header > h4").text
        p = popup.find_element(By.CSS_SELECTOR, "div.modal-body > p").text

        assert h4 == "It’s that Easy!! Well I think it is....."
        assert p == ("We can inject and use JavaScript code if all else fails! Remember always try to use WebDriver "
                     "Library method(s) first such as WebElement.click(). (The Selenium development team have spent "
                     "allot of time developing WebDriver functions etc).")

        popup.find_element(By.CSS_SELECTOR, "div.modal-footer > button").click()

        time.sleep(2)
