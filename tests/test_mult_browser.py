import time

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager


class TestMultBrowser:

    @pytest.fixture(params=["chrome", "edge"], scope="class")
    def driver(self, request):

        if request.param == "chrome":
            driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

        elif request.param == "edge":
            driver = webdriver.Edge(service=Service(EdgeChromiumDriverManager().install()))

        else:
            raise ValueError("Unsupported browser")

        yield driver
        driver.quit()

    def test1(self, driver):
        driver.get("http://techcanvass.com/Examples/webtable.html")
        driver.maximize_window()
        time.sleep(1)

    def test2(self, driver):
        driver.get("http://techcanvass.com/Examples/webtable.html")
        driver.maximize_window()
        time.sleep(1)

    def test3(self, driver):
        driver.get("http://techcanvass.com/Examples/webtable.html")
        driver.maximize_window()
        time.sleep(1)
